// 1. import/require http from node.js (http is what allow us to create a server)

let http = require('http');
// 2. Declare a variable assigned with a number value. This will be the port to be used or listened to by the server.

let port = 4000
// 3. Use http to create a server and write a header, then returns data (like'Hello World' string)
http.createServer(function(request, response){
	// 4. Write the header for the response specifying its content and status
	response.writeHead(200, {'Content-Type': 'text/plain'})
// 5. Ends the function and reurn a string with 'Hello World'
	response.end('Hello World')
	// 6. Attach a port to the server specifying where ou can access the server
}).listen(port)
// 7. Let the terminal/console know that the server is running on which port.
console.log(`Server is running at a localhost: ${port}`);